![profil](./assets/profil.png)

[@cy_hue](https://twitter.com/cy_hue) - [https://horsty.fr](https://horsty.fr)

## Créer son design system et l'utiliser ?

---

### Design system kezako ?

<img height=500 src="./assets/DesignSystem.png">

Note: design system :

- point de vue developpeur
- un outil
- aide au developpeur
- outil à la compréhension
- d'autre avantage, notamment UI/UX
- Atomic design
- convention de denomination (atome, molécule, organisme / type d'element / variation)

---

### Contraintes

<img height=500 src="./assets/contrainte.png">

Note:

- Webpack c'est non !
- Projet doit être privé
- Le moins cher car projet perso
- Hebergement possible ?

---

### Outils ?

<img height=500 src="./assets/tools.png">

Note:

- Storybook - back à sable
- RollupJs - librairie pour bundler en ESModule (ES6 compatible)
- ViteJs - Dev server / build command / esbuild (GO)
- Gitlab CI/CD
- Pnpm - Gestionnaire de packages rapide et économe en espace disque

---

[Mon storybook](https://horsty.gitlab.io/design-system/?path=/story/example-logo--inline)

[@storybook/builder-vite](https://github.com/storybookjs/builder-vite)

[Vite build library](https://vitejs.dev/guide/build.html#library-mode)

Note:

- addons utilise webpack (@storybook/addon-a11y)
- features storyStoreV7 use Webpack pour lazyloading
- pnpm --shamefully-hoist dependencies non déclaré dans le package.json
- CI_JOB_TOKEN default job variable https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
- rollup format ES for Es modules, treeshakable, remove unnecessary code import \* from foo
- rollup format UMD - Fallback compatibilité webpack/rollup
- CommonJs == nodeJs require()

---

![faq](./assets/gif-explain.gif)

---

### Ressources

- [Storybook et Vite](https://storybook.js.org/blog/storybook-for-vite/)
- [Clever cloud](https://www.clever-cloud.com/doc/clever-components/?path=/story/%F0%9F%8F%A0-home-readme--page)
- [Boilerplate](https://gitlab.com/Horsty/boilerplate-storybook-react-typescript-vite-pnpm)
- [Resolution de missing dependencies](https://pnpm.io/fr/faq#solution-2)
- [Gitlab Cache policy](https://docs.gitlab.com/ee/ci/yaml/index.html#cachepolicy)
- [Design system de W3C](https://design-system.w3.org)
- [Design system de firefox](https://design.firefox.com/photon/)
- [Liste de design system français](https://www.designsystems.fr/liste-des-designs-systems-francais)

---

### Bonus

TailwindCss

Note:

- JIT == onDemand plutot que de tout compiler
- preflight prevent injection style
